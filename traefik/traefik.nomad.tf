resource "nomad_job" "traefik" {
  jobspec = file("${path.module}/traefik_job.hcl")
  detach  = false  
}
