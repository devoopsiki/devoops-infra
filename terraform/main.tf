resource "digitalocean_droplet" "nomad-1" {
    image = "ubuntu-22-04-x64"
    name = "nomad-1"
    region = "fra1"
    size = "s-4vcpu-8gb"
    user_data = "${file("./user-config")}"
}

output "droplet_ip_address" {
  value = digitalocean_droplet.nomad-1.ipv4_address
}
