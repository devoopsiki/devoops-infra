#!/usr/bin/env bash

cat << EOF > ./DevOops-borscht-project/.env
IP_HOST=$1
GROUP_PORT=$2
IMAGE_TAG=$3
EOF

docker-compose -f ./DevOops-borscht-project/shop.yaml pull
docker-compose -f ./DevOops-borscht-project/shop.yaml up -d
