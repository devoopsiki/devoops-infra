job "gitlab-runner" {
  datacenters = ["p52"]
  type        = "service"
  
  group "gitlab-runner" {
    count = 1
    
    task "gitlab-runner" {
      driver = "docker"

      config {
        image = "gitlab/gitlab-runner:latest"
        
        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock",
          // "/data/gitlab-runner:/etc/gitlab-runner",
        ]
      }
      
      env {
        CI_SERVER_URL  = "https://gitlab.com/"
        REGISTRATION_TOKEN = "glrt-HwdxDNk5zbxxGS5m5b68"
        RUNNER_NAME = "nomad-runner-${NOMAD_TASK_NAME}"
      }
    }
  }
}
